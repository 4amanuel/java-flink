
Repo- branch convention
    - master
    - develop
    - develop/feature_xxxx
Requriments:
    - docker and docker-compose 
    - maven 3.6 or up
    - jdk 1.8
    - plenty of hair/coffee so you can pull/drink them out later :D just incase



Project uses apache-flink v1.9  configured in pom.xml

#####Docker-compose 
`docker-compose.yml` in the project directory.
Step 2: Update the -volume mappings by targeting the target forder in the project
Step 3: Map the sample data ingestion point into the docker-compose.yml 
prepare flink v1.9 docker image 
    `docker-compose up` 

#####To check for outputs
    docker exec -it <continer-id> flink run <examples/batch/demo/>java-flink-1.0-SNAPSHOT.jar



Step 1: import `pom.xml` and its folders into your IDE



#####Building fat jar
`mvn clean install` for first time, `mvn clean package` regular ,to resolve dependency issues `mvn dependency:resolve` 

#####Maven build profiles
(default) basic-build skips tests
        `mvn clean package`

with test (not added yet IT+Unit)
    `mvn -P basic-test clean package`



Progress:
1) pure flink based data ingestion - ok
2) flink Dataset data processing/filter - ok 
3) data export - not yet