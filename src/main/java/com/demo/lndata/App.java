package com.demo.lndata;

import java.nio.file.Files;


import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.formats.json.JsonNodeDeserializationSchema;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ArrayNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.util.Collector;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Hello world- flink!
 *
 */
public class App {

	public static final String DEMO_FILE = "/opt/flink/examples/batch/demo-data/Sidewalk_201912.GeoJSON";
	public static final double MIN_ROAD_LENGTH = 100;
	
	public static void main(String[] args) throws Exception {
		// set up the batch execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		
		List<Tuple6<String,String,String,String,String,Double>> tupcol = new ArrayList<>();
		JsonNodeDeserializationSchema demo = new JsonNodeDeserializationSchema();
		ObjectNode nodeJson = demo.deserialize(Files.readAllBytes(Paths.get(DEMO_FILE)));

	    ArrayNode entities = nodeJson.withArray("features");
	    for(JsonNode val2 : entities) {
	    	 JsonNode prop = val2.get("properties");
	    	 tupcol.add(new Tuple6<String,String,String,String,String,Double>(
						prop.get("COUNTY_NA").asText("empty"),
						prop.get("VILL_NAME").asText("empty"),
						prop.get("NAME").asText("empty"),
						prop.get("PSTART").asText("empty"),
						prop.get("PEND").asText("empty"),
						prop.get("LENGTH").asDouble(0)
						));
	    }
	    
	    DataSet<Tuple6<String,String,String,String,String,Double>> parsed = env.fromCollection(tupcol);
	    DataSet<Tuple6<String,String,String,String,String,Double>> parsed2 = parsed.filter(new RoadFilter());
	    DataSet<Tuple2<String,Integer>> bycity = parsed2.
	    		groupBy(0).reduceGroup(new CountyGroupReduce());
	    
	    DataSet<Tuple3<String,String,Integer>> byVillage = parsed2.groupBy(0, 1)
	    		.sortGroup(1, Order.ASCENDING)
	    		.reduceGroup(new VillageGroupReduce());
	    
	    
	    bycity.print();
	    byVillage.print();
	    
	    // byVillage.writeAsCsv("file:///opt/flink/examples/batch/demo-data/village.csv",WriteMode.OVERWRITE);
	    
	    System.out.println("Total values" + parsed.count());
	    System.out.println("Total values" + parsed2.count());
	    


		
	}
	 public static class RoadFilter implements FilterFunction<Tuple6<String,String,String,String,String,Double>> {
		  /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		  public boolean filter(Tuple6<String,String,String,String,String,Double> val) {
		    return val.f5 >= MIN_ROAD_LENGTH && !(val.f3.equalsIgnoreCase(val.f4));
		  }
		}
	 public static class CountyGroupReduce implements GroupReduceFunction<Tuple6<String,String,String,String,String,Double>, Tuple2<String, Integer>> {
			/**
		 * 
		 */
		private static final long serialVersionUID = -7624144127439555229L;

			@Override
			public void reduce(Iterable<Tuple6<String,String,String,String,String,Double>> values, Collector<Tuple2<String, Integer>> out) throws Exception {
				int sum = 0;
				String name = "";
				for (Tuple6<String,String,String,String,String,Double> value : values) {
					sum++;
					name = value.f0;
				}
				out.collect(new Tuple2<String,Integer> (name,sum));
			}
		}
	 public static class VillageGroupReduce implements GroupReduceFunction<Tuple6<String,String,String,String,String,Double>, Tuple3<String,String, Integer>> {
			/**
		 * 
		 */
		private static final long serialVersionUID = -8920230078641543581L;

			/**
		 * 
		 */
		

			@Override
			public void reduce(Iterable<Tuple6<String,String,String,String,String,Double>> values, Collector<Tuple3<String,String, Integer>> out) throws Exception {
				int sum = 0;
				String name = "",name2 = "";
				for (Tuple6<String,String,String,String,String,Double> value : values) {
					sum++;
					name = value.f0;
					name2 = value.f1;
				}
				out.collect(new Tuple3<String,String,Integer> (name,name2,sum));
			}
		}
	
}

@JsonIgnoreProperties(ignoreUnknown = true)
class ListGeoJson {
	public GeoJson[] features;
	
}


@JsonIgnoreProperties(ignoreUnknown = true)
class GeoJson {
	public Properties properties;

	public String toString() {
		return properties.toString();
	}

}

@JsonIgnoreProperties(ignoreUnknown = true)
class Properties {
	public String COUNTRY_NA;
	public String VILL_NAME;
	public String NAME;
	public String PSTART;
	public String PEND;
	public double LENGTH;

	public String toString() {
		return PSTART + " " + PEND + " " + LENGTH;
	}

}

